"""Event module used to implement cloudevents"""

from cloudevents.http import CloudEvent
from cloudevents.conversion import to_json

VERDICT = "verdict"

class Event(CloudEvent):
    """The Event object which wraps cloudevent functionality.

    Args: data [dict]: The data to build the event from.
    """

    SOURCE = "/spamcheck"
    NAMESPACE = "com.gitlab.spamcheck"

    def __init__(self, _type: str, data: dict):
        attributes = {
            "type": f"{self.NAMESPACE}.{_type}",
            "source": self.SOURCE,
        }
        super().__init__(attributes=attributes, data=data)

    def json(self) -> bytes:
        """Convert Event to a JSON byte array."""
        return to_json(self)

"""Process an issue to determine if it is spam or not."""
import api.v1.spamcheck_pb2 as spam

from app import logger
from app.spammable import Spammable
from server.interceptors import SpamCheckContext


log = logger.logger

# Expecting a module to exist in the directory specified by the ml_classifiers config option.
# i.e {ml_classifiers}/issue/ml
try:
    from issue import classifier
except ModuleNotFoundError as exp:
    log.warning("issue ML classifier not loaded", extra={"error": exp})
    classifier = None  # pylint: disable=invalid-name


class Issue(Spammable):
    """Analyze a GitLab issue to determine if it is spam."""

    def __init__(self, issue: spam.Issue, context: SpamCheckContext) -> None:
        super().__init__(issue, context, classifier)

Issue.set_max_verdict()

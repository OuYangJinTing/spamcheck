"""Process an snippet to determine if it is spam or not."""
from google.protobuf.json_format import MessageToDict

import api.v1.spamcheck_pb2 as spam

from app import logger
from app.spammable import Spammable
from server.interceptors import SpamCheckContext


log = logger.logger

# The generic classifier defaults to using the issue model since it has the most training data.
try:
    from issue import classifier
except ModuleNotFoundError as exp:
    log.warning("generic ML classifier not loaded", extra={"error": exp})
    classifier = None  # pylint: disable=invalid-name


class Generic(Spammable):
    """Analyze a generic spammable to determine if it is spam."""

    def __init__(self, spammable: spam.Generic, context: SpamCheckContext) -> None:
        super().__init__(spammable, context, classifier)

    def to_dict(self) -> dict:
        """Return the dictionary representation of the spammable."""
        spammable_dict = MessageToDict(self._spammable)
        spammable_dict["correlation_id"] = str(self.context.correlation_id)
        spammable_dict["title"] = ""
        spammable_dict["description"] = spammable_dict.pop("text")
        return spammable_dict

    def type(self) -> str:
        s_type = self._spammable.type

        if s_type == "":
            s_type = "Generic"

        return s_type

Generic.set_max_verdict()

"""Process an snippet to determine if it is spam or not."""
import api.v1.spamcheck_pb2 as spam

from app import logger
from app.spammable import Spammable
from server.interceptors import SpamCheckContext


log = logger.logger

# Expecting a module to exist in the directory specified by the ml_classifiers config option.
# i.e {ml_classifiers}/snippet/ml
try:
    from snippet import classifier
except ModuleNotFoundError as exp:
    log.warning("snippet ML classifier not loaded", extra={"error": exp})
    classifier = None  # pylint: disable=invalid-name


class Snippet(Spammable):
    """Analyze a GitLab snippet to determine if it is spam."""

    def __init__(self, snippet: spam.Snippet, context: SpamCheckContext) -> None:
        super().__init__(snippet, context, classifier)

Snippet.set_max_verdict()

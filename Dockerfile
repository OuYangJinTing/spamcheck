##############################
# Build Image
##############################
FROM python:3.9 as builder

ENV PIPENV_VENV_IN_PROJECT 1

# For some reason pip thinks uname is in /usr/local/bin when running in arm but it is in /bin
ENV PATH /bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin

# Install pipenv and other dependencies
RUN pip install pipenv

# Install python dependencies in /.venv and generate gRPC code
COPY . .
RUN pipenv install --deploy && make proto

# Make a src directory to avoid multiple copy layers in the runtime image
RUN mkdir ./src && \
  mv api app server *.py ./src

##############################
# Runtime Image
##############################
FROM python:3.9-slim as runtime

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

# Set minimum log level in tensorflow to error
# This will cause GPU warnings to be omitted
ENV TF_CPP_MIN_LOG_LEVEL 2

ARG UID=1000
ARG GID=1000
ARG CLASSIFIER_VERSION=3.0.1

# Copy python environment from builder
COPY --from=builder /.venv /.venv
ENV PATH="/.venv/bin:$PATH"

WORKDIR /spamcheck

# Copy code into runtime image
COPY --from=builder /src .
ADD "https://storage.googleapis.com/glsec-spamcheck-ml-artifacts/spam-classifier/${CLASSIFIER_VERSION}/gl-spam-classifier-${CLASSIFIER_VERSION}.tar.gz" ./classifiers.tar.gz

RUN tar xvf classifiers.tar.gz && rm classifiers.tar.gz

# Add non-root user to run spamchecklications
# Run chmod to ensure everything works if container is run with different uid
RUN groupadd -g $GID spamcheck && \
  adduser \
  --gecos "" \
  --disabled-password \
  --shell "/sbin/nologin" \
  --no-create-home \
  --gid $GID \
  spamcheck && \
  chown -R spamcheck:spamcheck . && \
  chmod -R u=rw,g=rw,o=r,a+X .

# Add spamcheck and health-check to path
RUN ln -s /spamcheck/main.py /usr/local/bin/spamcheck && \
  ln -s /spamcheck/health_check.py /usr/local/bin/health-check && \
  chmod 755 main.py health_check.py

USER spamcheck

# Export gRPC port
EXPOSE 8001

ENTRYPOINT ["spamcheck"]

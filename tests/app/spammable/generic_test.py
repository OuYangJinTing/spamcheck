import unittest
from unittest.mock import patch, PropertyMock

import api.v1.spamcheck_pb2 as spam
from app import config
from app.spammable import generic
from tests.app.helpers import MockContext, MockML


class TestGeneric(unittest.TestCase):
    def test_generic_attrs(self):
        obj = spam.Generic(text="test", type="random_generic")
        g = generic.Generic(obj, MockContext())

        to_dict = g.to_dict()
        self.assertEqual(
            "test", to_dict["description"], "Generic description not set correctly"
        )
        self.assertEqual(
            "random_generic", g.type(), "Generic type not set correctly"
        )

    def test_verdict_project_not_allowed(self):
        g = generic.Generic(spam.Generic(text="test"), MockContext())
        g._project_allowed = False
        v = g.verdict()
        self.assertEqual(
            spam.SpamVerdict.NOOP, v.verdict, "Disallowed project should return NOOP"
        )

    def test_score(self):
        g = generic.Generic(spam.Generic(text="test"), MockContext())

        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            g.calculate_verdict(0.39),
            "Confidence less than 0.4 should be allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            g.calculate_verdict(0.41),
            "Confidence between 0.4 and 0,5 should be allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            g.calculate_verdict(0.55),
            "Confidence between 0.5 and 0.9 should be allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            g.calculate_verdict(0.9),
            "Confidence of 0.9 or greater should be allowed",
        )

    def test_verdict(self):
        generic.classifier = MockML(1.0)
        g = generic.Generic(spam.Generic(text="test"), MockContext())
        g._project_allowed = False
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            g.verdict().verdict,
            "Disallowed project should return NOOP",
        )
        g._project_allowed = True
        g._email_allowed = True
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            g.verdict().verdict,
            "Allowed email should return ALLOW",
        )
        g._email_allowed = False
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            g.verdict().verdict,
            "ML inference of 1.0 should be allowed",
        )
        generic.classifier.set_score(0.1)
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            g.verdict().verdict,
            "ML inference of 0.1 should be allowed",
        )

    def test_verdict_no_ml(self):
        generic.classifier = None
        g = generic.Generic(spam.Generic(text="test"), MockContext())
        g.project_allowed = True
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            g.verdict().verdict,
            "Generic ML not loaded should return NOOP",
        )

    def test_generic_property(self):
        g = generic.Generic(spam.Generic(text="spam"), MockContext())
        g.allowed_domains = {"gitlab.com"}
        self.assertEqual("spam", g.spammable.text, "Generic text should have been set")
        self.assertEqual(False, g._email_allowed, "Blank email should not be allowed")
        args = {"user": {"emails": [{"email": "test@gitlab.com", "verified": True}]}}
        new_generic = spam.Generic(**args)
        g.spammable = new_generic
        self.assertEqual(
            True,
            g._email_allowed,
            "Email should be allowed after updating issue",
        )

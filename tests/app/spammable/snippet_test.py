import unittest
from unittest.mock import patch, PropertyMock

import api.v1.spamcheck_pb2 as spam
from app import config
from app.spammable import snippet
from tests.app.helpers import MockContext, MockML

class TestSnippet(unittest.TestCase):
    def test_verdict_project_not_allowed(self):
        s = snippet.Snippet(spam.Snippet(title="test"), MockContext())
        s._project_allowed = False
        v = s.verdict()
        self.assertEqual(
            spam.SpamVerdict.NOOP, v.verdict, "Disallowed project should return NOOP"
        )

    def test_score(self):
        s = snippet.Snippet(spam.Snippet(title="test"), MockContext())

        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            s.calculate_verdict(0.39),
            "Confidence less than 0.4 should be allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.51),
            "Confidence between 0.5 and 0.9 should be conditionally allowed",
        )
        # Due to valse positives all non allow verdicts were converted to `CONDITIONAL_ALLOW`
        # https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/191
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.95),
            "Confidence between 0.9 and 0.99 should be conditionally allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.999),
            "Confidence of 0.99 or greater should be conditionally allowed",
        )

    def test_verdict(self):
        snippet.classifier = MockML(1.0)
        s = snippet.Snippet(spam.Snippet(title="test"), MockContext())
        s._project_allowed = False
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            s.verdict().verdict,
            "Disallowed project should return NOOP",
        )
        s._project_allowed = True
        s._email_allowed = True
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            s.verdict().verdict,
            "Allowed email should return ALLOW",
        )
        s._email_allowed = False
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.verdict().verdict,
            "ML inference of 1.0 should be conditionally allowed",
        )
        snippet.classifier.set_score(0.1)
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            s.verdict().verdict,
            "ML inference of 0.1 should be allowed",
        )

    def test_verdict_no_ml(self):
        snippet.classifier = None
        s = snippet.Snippet(spam.Snippet(title="test"), MockContext())
        s.project_allowed = True
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            s.verdict().verdict,
            "Snippet ML not loaded should return NOOP",
        )


import unittest
from unittest.mock import patch, PropertyMock

import api.v1.spamcheck_pb2 as spam
from app import config
from app.spammable import issue
from tests.app.helpers import MockContext, MockML

class TestIssue(unittest.TestCase):
    def test_verdict_project_not_allowed(self):
        i = issue.Issue(spam.Issue(title="test"), MockContext())
        i._project_allowed = False
        v = i.verdict()
        self.assertEqual(
            spam.SpamVerdict.NOOP, v.verdict, "Disallowed project should return NOOP"
        )

    def test_score(self):
        s = issue.Issue(spam.Issue(title="test"), MockContext())

        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            s.calculate_verdict(0.49),
            "Confidence less than 0.5 should be allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.51),
            "Confidence between 0.5 and 0.9 should be conditionally allowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.95),
            "Confidence between 0.9 and 0.99 should be disallowed",
        )
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            s.calculate_verdict(0.999),
            "Confidence of 0.99 or greater should be blocked",
        )

    def test_verdict(self):
        issue.classifier = MockML(1.0)
        i = issue.Issue(spam.Issue(title="test"), MockContext())
        i._project_allowed = False
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            i.verdict().verdict,
            "Disallowed project should return NOOP",
        )
        i._project_allowed = True
        i._email_allowed = True
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            i.verdict().verdict,
            "Allowed email should return ALLOW",
        )
        i._email_allowed = False
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW,
            i.verdict().verdict,
            "ML inference of 1.0 should be conditionally allowed",
        )
        issue.classifier.set_score(0.1)
        self.assertEqual(
            spam.SpamVerdict.ALLOW,
            i.verdict().verdict,
            "ML inference of 0.1 should be allowed",
        )

    def test_verdict_no_ml(self):
        issue.classifier = None
        i = issue.Issue(spam.Issue(title="test"), MockContext())
        i.project_allowed = True
        self.assertEqual(
            spam.SpamVerdict.NOOP,
            i.verdict().verdict,
            "Issue ML not loaded should return NOOP",
        )

    def test_issue_property(self):
        i = issue.Issue(spam.Issue(title="spam"), MockContext())
        i.allowed_domains = {"gitlab.com"}
        self.assertEqual("spam", i.spammable.title, "Issue should have been set")
        self.assertEqual(False, i._email_allowed, "Blank email should not be allowed")
        args = {"user": {"emails": [{"email": "test@gitlab.com", "verified": True}]}}
        new_issue = spam.Issue(**args)
        i.spammable = new_issue
        self.assertEqual(
            True,
            i._email_allowed,
            "Email should not be allowed after updating issue",
        )

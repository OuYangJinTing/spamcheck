class MockContext:
    def __init__(self):
        self.correlation_id = "test"


class MockML:
    def __init__(self, score):
        self._score = score

    def score(self, args):
        return self._score

    def set_score(self, score):
        self._score = score

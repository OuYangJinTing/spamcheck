#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

kill_server() {
  kill $SPAMCHECK_PID
}

gem uninstall spamcheck
gem install ./spamcheck*.gem

python main.py --log-level debug &
SPAMCHECK_PID=$!

trap kill_server EXIT

# Wait for service to start
sleep 3
LOCAL_TESTING=TRUE ruby ./tests/integration/run.rb

exit 0
